# 图片压缩程序

#### 项目介绍
- 图片压缩程序

#### 安装环境
- golang 1.11

#### 目录文件
- 根目录/log  日志目录
- 根目录/temp  临时文件目录
- 根目录/main.go 程序源文件

#### 编译流程
- CD 根目录
- 执行 go build
- 根目录生成可执行文件，无需依赖外部扩展

#### 配置信息修改
- main.go 文件第47行
- OssCfg OSS配置
- RedisCfg Redis配置

#### 实现原理
- 服务方：go程序监听redis中list结构数据，类似消息队列入队出队的方式处理JSON数据
- 应用方：只需要往指定的list中添加JSON数据即可

#### JSON数据格式
~~~
{
    "taskId":"任务id",
    "fileName":"压缩包名称.zip",
    "callBack":"处理成功回调地址（?ossFile=压缩包oss完整地址）",
    "files":[
        {
            "ossFile":"uploads/117/b75/xxxx.jpg",
            "savePath":"压缩包根目录/子目录1/文件名.jpg"
        },
        {
            "ossFile":"uploads/e7d/28e/xxxxx.jpg",
            "savePath":"压缩包根目录/子目录2/文件名.jpg"
        },
        {
            "ossFile":"uploads/e7d/28e/xxxx.jpg",
            "savePath":"压缩包根目录/文件名.jpg"
        }
    ]
}
~~~




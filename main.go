package main

import (
	"fmt"
	"time"
	"github.com/garyburd/redigo/redis"
	"encoding/json"
	"os"
	"log"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"path/filepath"
	"strings"
	"archive/zip"
	"io"
	url2 "net/url"
	"net/http"
)


type JsonData struct {
	TaskId string
	FileName string
	CallBack string
	Files []FileObject
}

type FileObject struct {
	OssFile string
	SavePath string
}

type OssObject struct {
	Endpoiont string
	AccessKeyId string
	AccessKeySecret string
	BucketName string
	Cdn string
}

type RedisObject struct {
	Address string
	Auth string
	GetTaskKey string
	SetTaskKey string
}

var (
	BaseDir = "./temp"
	LockKey = "gozipLock"
	OssCfg = OssObject{
		"http://oss-cn-hangzhou-internal.aliyuncs.com",
		"LTAIMQOM4qz7b3Dc",
		"QeoudH9nGiuMQvGoPxTunwu67hVyAq",
		"dy-allimage",
		"https://dy-allimage.oss-cn-hangzhou.aliyuncs.com",
	}
	RedisCfg = RedisObject{
		"172.16.136.141:6379",
		//"47.97.223.29:6379",
		"dyyxClub888",
		"goTaskQueues",
		"goTaskAcks",
	}
)

func init() {
	//设置日志输出到error.log文件
	CreateDir("./log")
	file, _ := os.OpenFile("./log/error.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	//defer file.Close()
	log.SetOutput(file)
	log.SetPrefix("[Error]")
	log.SetFlags(log.Ldate|log.Ltime |log.LUTC)
}

func main() {
	fmt.Println("开始监听压缩队列数据：key -> ", RedisCfg.Address , RedisCfg.GetTaskKey)
	timer(timerFunc)
}

func timerFunc() {
	lock, _ := getRedisData(LockKey)
	if lock != "" {
		fmt.Printf("lock error : %s", lock)
		return
	}
	//获取任务数据
	data, err := getTaskData()
	if err == nil {
		go initZip(data)
	}
}

func timer(timer func()) {
	ticker := time.NewTicker(3*time.Second)
	for {
		select {
		case <-ticker.C:
			timer()
		}
	}
}

func initZip(Data JsonData) {
	setRedisData(LockKey, Data.TaskId)
	fmt.Println("开始处理任务-----TaskId: ", Data.TaskId)
	fmt.Println(Data)
	//创建临时目录
	var tempDar = BaseDir + "/file_" + Data.FileName
	CreateDir(tempDar)

	// 创建OSSClient实例。
	client, err := oss.New(OssCfg.Endpoiont, OssCfg.AccessKeyId, OssCfg.AccessKeySecret)
	if err != nil {
		log.Println("OSS Error:", err)
		os.Exit(-1)
	}
	// 获取存储空间。
	bucket, err := client.Bucket(OssCfg.BucketName)
	if err != nil {
		log.Println("OSS Error:", err)
		os.Exit(-1)
	}
	files := make([]string, 0)
	//遍历下载图片到本地
	for _, item := range Data.Files {
		// 下载文件到本地文件。
		filePath := tempDar + "/"
		if strings.Contains(item.SavePath, "/") == true {
			name := filepath.Base(item.SavePath)
			filePath += strings.Replace(item.SavePath, "/" + name, "", 1)
			CreateDir(filePath)
			filePath += "/" + name
		} else {
			filePath += item.SavePath
		}
		err = bucket.GetObjectToFile(item.OssFile, filePath)
		if err != nil {
			log.Println("OSS Error:", err)
		} else {
			files = append(files, filePath)
		}
	}
	//图片压缩 zip 格式
	zipFileName := BaseDir + "/" + Data.FileName
	enZip(zipFileName, files, tempDar)
	// 分片大小10Mb，3个协程并发上传分片，使用断点续传。
	// 其中"<yourObjectName>"为objectKey， "LocalFile"为filePath，10*1024*1024为partSize。
	objectKey := "gozip/"+Data.FileName
	err = bucket.UploadFile(objectKey, zipFileName, 10*1024*1024, oss.Routines(3), oss.Checkpoint(true, ""))
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}
	//删除本地图片
	os.RemoveAll(zipFileName)
	destorytemp(tempDar)
	//处理完成发送通知
	ossFile := OssCfg.Cdn + "/" + "gozip/"+url2.QueryEscape(Data.FileName)
	callBackFun(Data.CallBack, Data.TaskId, ossFile)
	setTaskMessage(Data.TaskId, ossFile)
	fmt.Printf("完成处理任务-----TaskId: %s , objectKey: %s \n", Data.TaskId, objectKey)
	setRedisData(LockKey, "")
}

func callBackFun(url string, taskId string, ossFile string) {
	p := url2.Values{}
	p.Add("taskId", taskId)
	p.Add("ossFile", ossFile)
	url += "?" + p.Encode()
	_, err := http.Get(url)
	fmt.Println(url)
	if err != nil {
		fmt.Println("callBackError:", err)
	}
}

//生成zip文件
func enZip(zipFile string, fileList []string, tempFile string) error {
	//创建 zip 文件
	fw, err := os.Create(zipFile)
	if err != nil {
		log.Println("os Error:", err)
	}
	defer fw.Close()
	zw := zip.NewWriter(fw)
	defer func() {
		// 检测一下是否成功关闭
		if err := zw.Close(); err != nil {
			log.Println("os Error:", err)
		}
	}()
	for _, fileName := range fileList {
		fr, err := os.Open(fileName)
		if err != nil {
			return err
		}
		fi, err := fr.Stat()
		if err != nil {
			return err
		}
		// 写入文件的头信息
		fh, err := zip.FileInfoHeader(fi)
		fh.Method = zip.Deflate
		fh.Name = strings.Replace(fileName, tempFile + "/", "", 1)
		w, err := zw.CreateHeader(fh)
		if err != nil {
			return err
		}
		// 写入文件内容
		_, err = io.Copy(w, fr)
		fr.Close()
		if err != nil {
			return err
		}
	}
	return nil
}

//删除图片
func destorytemp(path string) {

	filepath.Walk(path, func(path string, fi os.FileInfo, err error) error {
		if nil == fi {
			return err
		}
		err2 := os.RemoveAll(path)
		if err2 != nil {
			log.Println("destorytemp Error:", err2)
		}
		return nil
	})

}


func CreateDir(dir string){
	err := os.MkdirAll(dir, os.ModePerm)
	if err!=nil{
		fmt.Println(err)
	}
}

func getRedisData(key string)(string, error){
	conn,err := redis.Dial("tcp", RedisCfg.Address)
	if err != nil {
		fmt.Println("redis error :",err)
		return "", err
	}
	defer conn.Close()
	if _, err := conn.Do("AUTH", RedisCfg.Auth); err != nil {
		conn.Close()
		fmt.Println("redis error :",err)
		return "", err
	}
	if _, err := conn.Do("SELECT", 1); err != nil {
		conn.Close()
		fmt.Println("redis error :",err)
		return "", err
	}
	data,err := redis.String(conn.Do("get", key))
	if err != nil {
		return "", err
	}
	return data, nil
}

func setRedisData(key string, value string)error{
	conn,err := redis.Dial("tcp", RedisCfg.Address)
	if err != nil {
		fmt.Println("redis error :",err)
		return err
	}
	defer conn.Close()
	if _, err := conn.Do("AUTH", RedisCfg.Auth); err != nil {
		conn.Close()
		fmt.Println("redis error :",err)
		return err
	}
	if _, err := conn.Do("SELECT", 1); err != nil {
		conn.Close()
		fmt.Println("redis error :",err)
		return err
	}
	if _, err := conn.Do("SET", key, value); err != nil {
		conn.Close()
		log.Println("redis error :",err)
		return err
	}
	return nil
}

func getTaskData()(JsonData, error){
	data := JsonData{}
	conn,err := redis.Dial("tcp", RedisCfg.Address)
	if err != nil {
		fmt.Println("redis error :",err)
		return data, err
	}
	defer conn.Close()
	if _, err := conn.Do("AUTH", RedisCfg.Auth); err != nil {
		conn.Close()
		fmt.Println("redis error :",err)
		return data, err
	}
	if _, err := conn.Do("SELECT", 1); err != nil {
		conn.Close()
		fmt.Println("redis error :",err)
		return data, err
	}
	//使用redis的string类型获取list数据
	jsonStr,err := redis.String(conn.Do("lpop", RedisCfg.GetTaskKey))
	if err != nil {
		return data, err
	}
	err2 := json.Unmarshal([]byte(jsonStr), &data)
	if err2 != nil {
		return data, err
	}
	return data, nil
}

func setTaskMessage(key string, value string)error{
	conn,err := redis.Dial("tcp", RedisCfg.Address)
	if err != nil {
		log.Println("redis error :",err)
		return err
	}
	defer conn.Close()
	if _, err := conn.Do("AUTH", RedisCfg.Auth); err != nil {
		conn.Close()
		log.Println("redis error :",err)
		return err
	}
	if _, err := conn.Do("SELECT", 1); err != nil {
		conn.Close()
		log.Println("redis error :",err)
		return err
	}
	if _, err := conn.Do("HSET", RedisCfg.SetTaskKey, key, value); err != nil {
		conn.Close()
		log.Println("redis error :",err)
		return err
	}
	return nil
}



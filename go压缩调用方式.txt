往redis 插入 json 数据 程序会自动监听处理，通知分为两种，callback 回调 和 监听redis消息  

redis 插入任务key ： goTaskQueues  数据类型为 list， json 插入方式为 lset   
reids 通知key：goTaskAcks  数据类型为 hash  格式为  key 任务id， value 压缩文件地址
callback 回调 参考下列json数据 callBack 字段

redis命令示例： LSET KEY_NAME INDEX VALUE



json数据


{"taskId":"task0001","fileName":"\u798f\u5dde\u57ce\u5e02\u4e4b\u5149\u6444\u5f71\u5927\u8d5b11.zip","callBack":"http:\/\/match.test.pai2345.com\/admin\/index\/uploadRs","files":[{"ossFile":"uploads\/117\/b75\/117b755cebeac727b26c9c65f876a02b.jpg","savePath":"\u53f0\u6c5f\u533a\/43136_\u7a0b\u5609\u9a8f_18850351817_\u5904\u5929\u7a7a\u4e4b\u4e0a\uff0c\u5fc6\u5de6\u6d77\u5f80\u4e8b_1.jpg"},{"ossFile":"uploads\/e7d\/28e\/e7d28ec1c8e1243595830b5fbf5e4953.jpg","savePath":"\u9759\u5b89\u533a\/444440_\u5f001.jpg"},{"ossFile":"uploads\/a7c\/f2a\/a7cf2a6dafd2552c568d635e3952d5cc.jpeg","savePath":"\u53f0\u6c5f\u533a\/444440_\u70e6\u70e6\u70e641.jpeg"}]}


回调地址
http://match.test.pai2345.com/admin/index/uploadRs?ossFile=https%3A%2F%2Fdy-allimage.oss-cn-hangzhou.aliyuncs.com%2Fgozip%2F%25E7%25A6%258F%25E5%25B7%259E%25E5%259F%258E%25E5%25B8%2582%25E4%25B9%258B%25E5%2585%2589%25E6%2591%2584%25E5%25BD%25B1%25E5%25A4%25A7%25E8%25B5%259B11.zip&taskId=task0001